#  _____   ___  ______ _   _   ___  _____  _____
# /  __ \ / _ \ | ___ \ \ | | / _ \|  __ \|  ___|
# | /  \// /_\ \| |_/ /  \| |/ /_\ \ |  \/| |__
# | |    |  _  ||    /| . ` ||  _  | | __ |  __|
# | \__/\| | | || |\ \| |\  || | | | |_\ \| |___
#  \____/\_| |_/\_| \_\_| \_/\_| |_/\____/\____/

# ZSHRC by Devoalda

[[ -f "$HOME/.zprofile" ]] && source "$HOME/.zprofile"

# Prompt
eval "$(starship init zsh)"

HISTFILE=~/.config/zsh/.zsh_history
autoload -Uz compinit
compinit
compinit -d ~/.cache/zsh/zcompdump-$ZSH_VERSION
autoload -U colors && colors	# Load colors
setopt autocd		# Automatically cd into typed directory.

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'left' vi-backward-char
bindkey -M menuselect 'down' vi-down-line-or-history
bindkey -M menuselect 'up' vi-up-line-or-history
bindkey -M menuselect 'right' vi-forward-char
bindkey -v '^?' backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# MOTD

HOSTNAME=$(hostname)
[ -e /usr/bin/figlet ] && figlet "$HOSTNAME" || echo "$HOSTNAME"
date "+%A %d %B %Y %T"

# Quotes
[ -f $HOME/.local/share/quotes ] && echo "" && shuf -n 1 "$HOME/.local/share/quotes"

# Pywal
# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
[ -f $HOME/.cache/wal/sequences ] && cat ~/.cache/wal/sequences

# Sources
# Aliases
for f in $HOME/.config/alias/*; do source "$f"; done
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
# Command not found for Arch/Manjaro
[ -f /usr/share/doc/pkgfile/command-not-found.zsh ] && source /usr/share/doc/pkgfile/command-not-found.zsh
# Command not found for ubuntu
[ -f  /etc/zsh_command_not_found ] && source  /etc/zsh_command_not_found
# Suggest aliases for commands
source /usr/share/zsh/plugins/zsh-you-should-use/you-should-use.plugin.zsh 2>/dev/null
# Syntax Highlighting
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null
# Auto Suggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
# thefuck
eval $(thefuck --alias)
