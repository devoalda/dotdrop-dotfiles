#!/bin/sh

[ -e /usr/bin/exa ] && LIST="exa" || LIST="ls"
[ -e /usr/bin/rip ] && DELF="rip" || DELF="rm"
[[ -e /usr/bin/doas && -e /etc/doas.conf ]] && RT="doas" || RT="sudo"

case "$LIST" in
	exa)
		alias ls='$LIST -lg --color=always --group-directories-first --icons'
		alias ll='$LIST -al --color=always --group-directories-first --icons'
		alias la='$LIST -agG --color=always --group-directories-first --icons'
		alias lt='$LIST -aT --color=always --group-directories-first'
		;;
	ls)
		alias ls='ls --color=auto'
		alias ll='ls -la'
		alias la='ls -a'
		alias l.='ls -d .* --color=auto'
		;;
esac

case "$DELF" in
	rip)
		alias rm='rip -i'
		alias rmls='rip -s'
		alias rmre='rip -u'
		alias rmrea='rip -su'
		;;

	rm)
		alias rm='rm -i'
		;;

esac

# Sudo/doas alias
case "$RT" in
	doas)
		alias sudo="doas -- "
		;;
	sudo)
		alias sudo="sudo"
		;;
esac

## get rid of command not found ##
alias cd..='cd ..'
## a quick way to get out of current directory ##
alias ..='cd ..'
alias ...='cd ../../../'
alias ....='cd ../../../../'
alias .....='cd ../../../../'
alias .4='cd ../../../../'
alias .5='cd ../../../../..'

alias dir='dir --color=auto'
alias grep='grep --color=auto'

# reboot / halt / poweroff
alias reboot='sudo /sbin/reboot'
alias poweroff='sudo /sbin/poweroff'
alias halt='sudo /sbin/halt'
alias shutdown='sudo /sbin/shutdown'

# Nerd aliases
alias :q='echo "Nerd..." && sleep 1 && exit'
alias q='exit'
alias :e='echo "Nerd..." && sleep 1 && nvim'

alias h='history'
alias hg='history | grep '
alias hgi='history | grep -i '
