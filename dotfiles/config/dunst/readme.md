<!-- PROJECT LOGO -->

![Wolf Logo](https://i.pinimg.com/originals/5e/0a/ac/5e0aacf4c75613ec814457d520050c88.jpg)

# Dunst Configuration By Devoalda

Devoalda's configuration for dunst notification daemon \
[Explore the docs](./readme.md)


## Table of Contents
* [Introduction](#introduction)
* [Installation](#installation)
* [Sources](#sources)


## Introduction
This folder contains my dunstrc configuration file as a symlink

## Installation
Run wal -R to restore configurations for pywal and to generate dunstrc in wal cache \
Symlink cache file to dunstrc in the config folder
```bash
* ln -sf "${HOME}/.cache/wal/dunstrc" "${HOME}/.config/dunst/dunstrc"
```

## Sources
