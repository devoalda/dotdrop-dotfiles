	" Markdown Plugins
	" Disables automatic commenting on newline:
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
	function! s:isAtStartOfLine(mapping)
	  let text_before_cursor = getline('.')[0 : col('.')-1]
	  let mapping_pattern = '\V' . escape(a:mapping, '\')
	  let comment_pattern = '\V' . escape(substitute(&l:commentstring, '%s.*$', '', ''), '\')
	  return (text_before_cursor =~? '^' . ('\v(' . comment_pattern . '\v)?') . '\s*\v' . mapping_pattern . '\v$')
	endfunction

	inoreabbrev <expr> <bar><bar>
		  \ <SID>isAtStartOfLine('\|\|') ?
		  \ '<c-o>:TableModeEnable<cr><bar><space><bar><left><left>' : '<bar><bar>'
	inoreabbrev <expr> __
		  \ <SID>isAtStartOfLine('__') ?
		  \ '<c-o>:silent! TableModeDisable<cr>' : '__'

	" Spell-check Markdown files and Git Commit Messages
	autocmd FileType markdown setlocal spell
	autocmd FileType gitcommit setlocal spell

	" Enable dictionary auto-completion in Markdown files and Git Commit Messages
	autocmd FileType markdown setlocal complete+=kspell
	autocmd FileType gitcommit setlocal complete+=kspell

	"Markdown header
	autocmd FileType markdown noremap <leader>r i---<CR>title:<Space><++><CR>author:<Space>"Devoalda"<CR>geometry:<CR>-<Space>top=30mm<CR>-<Space>left=20mm<CR>-<Space>right=20mm<CR>-<Space>bottom=30mm<CR>header-includes:<Space>\|<CR><Tab>\usepackage{float}<CR>\let\origfigure\figure<CR>\let\endorigfigure\endfigure<CR>\renewenvironment{figure}[1][2]<Space>{<CR><Tab>\expandafter\origfigure\expandafter[H]<CR><BS>}<Space>{<CR><Tab>\endorigfigure<CR><BS>}<CR><BS>---<CR><CR>
	let g:vim_markdown_new_list_item_indent = 0
	let g:vim_markdown_folding_disabled = 1
