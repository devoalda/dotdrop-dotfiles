" VimPlug install
if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

" VimPlug
call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))

	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	" Appearence
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	" Vim Status Bar
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'

	" Vim Colorscheme
	Plug 'dylanaraps/wal.vim'

	" CSS Color display
	"Plug 'ap/vim-css-color'
	Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }

	" Backup Colorscheme
	Plug 'dikiaap/minimalist'

	" Syntax Highlighting
	Plug 'vim-python/python-syntax'
	Plug 'yuezk/vim-js'
	Plug 'maxmellon/vim-jsx-pretty'
	Plug 'HerringtonDarkholme/yats.vim'
	Plug 'rust-lang/rust.vim'
	Plug 'vim-pandoc/vim-pandoc-syntax'

  " Zen Mode
  Plug 'mmai/vim-zenmode'

	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	" Syntax
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	" Code Language Pack
	Plug 'sheerun/vim-polyglot'

	" Code Completeter
	"Plug 'ycm-core/YouCompleteMe'
	Plug 'neoclide/coc.nvim', {'branch': 'release'}

	" Vim Snippets with YCM
	"Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

	" Generate YCM in file with :YcmGenerateConfig or :CCGenerateConfig
	"Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }

	" Bracket Surround with <C-s> <BRACKET>
	Plug 'tpope/vim-surround'

	" Comment Shortcut <leader>cc or <leader>cu
	" Plug 'preservim/nerdcommenter'
	Plug 'tpope/vim-commentary'

	" Line Numbers
	Plug 'jeffkreeftmeijer/vim-numbertoggle'

	" Multiple Selection with <C-n>
	Plug 'terryma/vim-multiple-cursors'

	" Vim align with <ga>
	Plug 'junegunn/vim-easy-align'

	"Markdown Plugins
	Plug 'godlygeek/tabular'
	Plug 'plasticboy/vim-markdown'
	Plug 'dhruvasagar/vim-table-mode'

	" Golang
	" Use with :GoBuild, :GoInstall, :GoTest, :GoRun
	Plug 'fatih/vim-go', { 'tag': '*' }

	" Golang Autocomplete
	Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }

	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	" Others
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	" NerdTree
	Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
	Plug 'ryanoasis/vim-devicons'

	" fzf
	Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
	Plug 'junegunn/fzf.vim'

  " vim which key
  Plug 'liuchengxu/vim-which-key'
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	" Unused Plugins
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	" Github dashboard with :GHDashboard (Requires ruby)
	" Plug 'junegunn/vim-github-dashboard'
	" Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
	" Plug 'itchyny/lightline.vim'

call plug#end()

" Auto install vim plugin
autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif
