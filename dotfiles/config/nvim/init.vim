""""""""""""""""""""""""""""""""""""""""""""""""""
"                                                "
" Init.vim/vimrc                                 "
" by Devoalda                                    "
"                                                "
""""""""""""""""""""""""""""""""""""""""""""""""""
source $HOME/.config/nvim/vim-plug/plugins.vim
source $HOME/.config/nvim/general/settings.vim
source $HOME/.config/nvim/keys/mappings.vim
source $HOME/.config/nvim/colors/colors.vim
source $HOME/.config/nvim/keys/which-key.vim

if exists('g:vscode')
  source $HOME/.config/nvim/vscode/windows.vim
else
 source $HOME/.config/nvim/plug-config/coc.vim
 source $HOME/.config/nvim/plug-config/fzf.vim
 source $HOME/.config/nvim/plug-config/markdown.vim
 source $HOME/.config/nvim/plug-config/vim-snippets.vim
 source $HOME/.config/nvim/plug-config/vim-commentary.vim
 source $HOME/.config/nvim/plug-config/easyalign.vim
 source $HOME/.config/nvim/plug-config/zenmode.vim
 source $HOME/.config/nvim/colors/airline.vim
 source $HOME/.config/nvim/colors/vim-hexokinase.vim
 source $HOME/.config/nvim/colors/colors.vim
 source $HOME/.config/nvim/general/code.vim
endif

