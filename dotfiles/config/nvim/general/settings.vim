set iskeyword+=-                      	" treat dash separated words as a word text object"
set formatoptions-=cro                  " Stop newline continution of comments

if !exists('g:vscode')
  set nowrap                              " Display long lines as just one line
  set encoding=utf-8                      " The encoding displayed
  set pumheight=10                        " Makes popup menu smaller
  syntax enable                           " Enables syntax highlighing
  set clipboard+=unnamedplus              " Copy paste between vim and everything else
  set mouse=nicr                          " Enable your mouse
  set splitbelow                          " Horizontal splits will automatically be below
  set splitright                          " Vertical splits will automatically be to the right
  set t_Co=256                            " Support 256 colors
  set conceallevel=0                      " So that I can see `` in markdown files
  set tabstop=2                           " Insert 2 spaces for a tab
  set shiftwidth=2                        " Change the number of space characters inserted for indentation
  set smarttab                            " Makes tabbing smarter will realize you have 2 vs 4
  set expandtab                           " Converts tabs to spaces
  set smartindent                         " Makes indenting smart
  set autoindent                          " Good auto indent
  set laststatus=2                        " Always display the status line
  set number relativenumber               " Line numbers
  set updatetime=300                      " Faster completion
  set timeoutlen=100                      " By default timeoutlen is 1000 ms
  set noshowmode                          " We don't need to see things like -- INSERT -- anymore
  set nobackup                            " This is recommended by coc
  set nowritebackup                       " This is recommended by coc
  set cmdheight=2                         " More space for displaying messages
  set shortmess+=c                        " Don't pass messages to |ins-completion-menu|.
  set signcolumn=yes                      " Always show the signcolumn, otherwise it would shift the text each time
  set incsearch
	set nu rnu
  set autoread
  set laststatus=2
  filetype plugin indent on
	set path+=**			                 		" Searches current directory recursively.
	set wildmode=longest,list,full
	set noswapfile
  set autochdir                           " Your working directory will always be the same as your working directory
  set hidden                              " Required to keep multiple buffers open multiple buffers
  set guifont=Hack

  " au! BufWritePost $MYVIMRC source %      " auto source when writing to init.vm alternatively you can run :source $MYVIMRC
  autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o


	" Remove Trailing Whitespaces on save
	autocmd BufWritePre * %s/\s\+$//e

  " You can't stop me
  cmap w!! w !sudo tee %

  " set ruler             			            " Show the cursor position all the time
  "set cursorline                         " Enable highlighting of the current line
  " set showtabline=2                     " Always show tabs
  " set background=dark                     " tell vim what the background color looks like
  " set fileencoding=utf-8                  " The encoding written to file
  " let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  " set mmp=1300
  " set foldcolumn=2                        " Folding abilities
  " set termguicolors
endif
