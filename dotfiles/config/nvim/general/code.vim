" JSON formatter
com! FormatJSON %!python -m json.tool

" Java
autocmd FileType java setlocal omnifunc=javacomplete#Complete

" Python syntax
let g:python_highlight_all = 1
