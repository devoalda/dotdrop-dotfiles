""""""""""""""""""""""""""""""""""""""""""""""""""
"                                                "
" Init.vim/vimrc                                 "
" by Devoalda                                    "
"                                                "
""""""""""""""""""""""""""""""""""""""""""""""""""

" VimPlug install
if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

if !exists('g:vscode')
	" VimPlug
	call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))

	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Appearence
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Vim Status Bar
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'

	" Vim Colorscheme
	Plug 'dylanaraps/wal.vim'

	" CSS Color display
	"Plug 'ap/vim-css-color'
	Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }

	" Backup Colorscheme
	Plug 'dikiaap/minimalist'

	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Syntax
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Code Language Pack
	Plug 'sheerun/vim-polyglot'

	" Code Completeter
	Plug 'ycm-core/YouCompleteMe'

	" Vim Snippets with YCM
	Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

	" Generate YCM in file with :YcmGenerateConfig or :CCGenerateConfig
	Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }

	" Bracket Surround with <C-s> <BRACKET>
	Plug 'tpope/vim-surround'

	" Comment Shortcut <leader>cc or <leader>cu
	Plug 'preservim/nerdcommenter'

	" Line Numbers
	Plug 'jeffkreeftmeijer/vim-numbertoggle'

	" Multiple Selection with <C-n>
	Plug 'terryma/vim-multiple-cursors'

	" Vim align with <ga>
	Plug 'junegunn/vim-easy-align'

	"Markdown Plugins
	Plug 'godlygeek/tabular'
	Plug 'plasticboy/vim-markdown'
	Plug 'dhruvasagar/vim-table-mode'
	
	" Golang
	" Use with :GoBuild, :GoInstall, :GoTest, :GoRun
	Plug 'fatih/vim-go', { 'tag': '*' }

	" Golang Autocomplete
	Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }

	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Others 
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" NerdTree
	Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
	Plug 'ryanoasis/vim-devicons'

	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Unused Plugins
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Github dashboard with :GHDashboard (Requires ruby)
	" Plug 'junegunn/vim-github-dashboard'
	" Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
	" Plug 'itchyny/lightline.vim'

	call plug#end()

	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Basic 
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" File Encoding
	set encoding=UTF-8

	" Line Number
	set number relativenumber
	set nu rnu

	" Spell 
	set spelllang=en_us

	" Mouse Scrolling
	set mouse=nicr

	" Use system clipboard
	set clipboard+=unnamedplus
	
	" Editing
	set autoread
	set laststatus=2
	filetype plugin indent on
	syntax on

	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Color 
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	"set t_Co=256
	"set notermguicolors t_Co=16
	set termguicolors
	silent! colorscheme wal 

	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Others 
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	set splitbelow splitright
	set path+=**					" Searches current directory recursively.
	set incsearch
	
	" Provide Tabcomplete for vim commands
	set wildmenu					" Display all matches when tab complete.
	set wildmode=longest,list,full

	" File backup settings
	set nobackup
	set noswapfile

	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Plugin Settings 
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Vim-Airline
	let g:airline_theme='jellybeans'

	if !exists('g:airline_symbols')
		let g:airline_symbols = {}
	endif
	let g:airline_symbols.space = "\ua0"

	" unicode symbols
	let g:airline_left_sep = '»'
	let g:airline_left_sep = '▶'
	let g:airline_right_sep = '«'
	let g:airline_right_sep = '◀'
	let g:airline_symbols.crypt = '🔒'
	let g:airline_symbols.linenr = '☰'
	let g:airline_symbols.linenr = '␊'
	let g:airline_symbols.linenr = '␤'
	let g:airline_symbols.linenr = '¶'
	let g:airline_symbols.maxlinenr = ''
	let g:airline_symbols.maxlinenr = '㏑'
	let g:airline_symbols.branch = '⎇'
	let g:airline_symbols.paste = 'ρ'
	let g:airline_symbols.paste = 'Þ'
	let g:airline_symbols.paste = '∥'
	let g:airline_symbols.spell = 'Ꞩ'
	let g:airline_symbols.notexists = 'Ɇ'
	
	" powerline symbols
	let g:airline_left_sep = ''
	let g:airline_left_alt_sep = ''
	let g:airline_right_sep = ''
	let g:airline_right_alt_sep = ''
	let g:airline_symbols.branch = ''
	let g:airline_symbols.readonly = ''
	let g:airline_symbols.linenr = '☰'
	let g:airline_symbols.maxlinenr = ''

	" Vim CSS Colors
	let g:cssColorVimDoNotMessMyUpdatetime = 1

	" Vim Snippets
	let g:UltiSnipsExpandTrigger="<tab>"
	let g:UltiSnipsJumpForwardTrigger="<c-b>"
	let g:UltiSnipsJumpBackwardTrigger="<c-z>"

	" Markdown Plugins
	" Disables automatic commenting on newline:
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
	function! s:isAtStartOfLine(mapping)
	  let text_before_cursor = getline('.')[0 : col('.')-1]
	  let mapping_pattern = '\V' . escape(a:mapping, '\')
	  let comment_pattern = '\V' . escape(substitute(&l:commentstring, '%s.*$', '', ''), '\')
	  return (text_before_cursor =~? '^' . ('\v(' . comment_pattern . '\v)?') . '\s*\v' . mapping_pattern . '\v$')
	endfunction

	inoreabbrev <expr> <bar><bar>
		  \ <SID>isAtStartOfLine('\|\|') ?
		  \ '<c-o>:TableModeEnable<cr><bar><space><bar><left><left>' : '<bar><bar>'
	inoreabbrev <expr> __
		  \ <SID>isAtStartOfLine('__') ?
		  \ '<c-o>:silent! TableModeDisable<cr>' : '__'

	" Spell-check Markdown files and Git Commit Messages
	autocmd FileType markdown setlocal spell
	autocmd FileType gitcommit setlocal spell

	" Enable dictionary auto-completion in Markdown files and Git Commit Messages
	autocmd FileType markdown setlocal complete+=kspell
	autocmd FileType gitcommit setlocal complete+=kspell
	
	"Markdown header
	autocmd FileType markdown noremap <leader>r i---<CR>title:<Space><++><CR>author:<Space>"Devoalda"<CR>geometry:<CR>-<Space>top=30mm<CR>-<Space>left=20mm<CR>-<Space>right=20mm<CR>-<Space>bottom=30mm<CR>header-includes:<Space>\|<CR><Tab>\usepackage{float}<CR>\let\origfigure\figure<CR>\let\endorigfigure\endfigure<CR>\renewenvironment{figure}[1][2]<Space>{<CR><Tab>\expandafter\origfigure\expandafter[H]<CR><BS>}<Space>{<CR><Tab>\endorigfigure<CR><BS>}<CR><BS>---<CR><CR>
	let g:vim_markdown_new_list_item_indent = 0
	let g:vim_markdown_folding_disabled = 1
	
	" Vim EasyAlign
	" Start interactive EasyAlign in visual mode (e.g. vipga)
	xmap ga <Plug>(EasyAlign)

	" Start interactive EasyAlign for a motion/text object (e.g. gaip)
	nmap ga <Plug>(EasyAlign)

	" NerdTree 
	map <C-f> :NERDTreeToggle<CR>
	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

	" JSON formatter
	com! FormatJSON %!python -m json.tool

	" Java
	autocmd FileType java setlocal omnifunc=javacomplete#Complete

	" Colors
	let g:Hexokinase_highlighters = ['foregroundfull']

	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
	" Unused Settings 
	""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	

	"if !has('nvim')
		"set viminfo+=%,<800,'10,/50,:100,h,f0,n~/.config/nvim/viminfo
	"endif
	" Let lightline do the work
	"set noshowmode
	"function! LightlineFilename()
	  "let filename = expand('%:t') !=# '' ? expand('%:t') : '[No Name]'
	  "let modified = &modified ? ' +' : ''
	  "return filename . modified
	"endfunction

	" Place Holders mapped to <C-j>
	map <C-j> <Esc>/<++><CR><Esc>cf>
endif
