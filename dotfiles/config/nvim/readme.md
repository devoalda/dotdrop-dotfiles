![Wolf Logo](https://i.pinimg.com/originals/5e/0a/ac/5e0aacf4c75613ec814457d520050c88.jpg)

# Neovim Congigurations By Devoalda

Devoalda's Neovim Configurations 
\
[Explore the docs](./readme.md)

# Dependencies
[Neovim](https://github.com/neovim/neovim)

# Installation
File [init.vim](./init.vim) will install vim-plug if not installed, however if you want to install, follow the command below.

Install vim-plug:
```bash
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

# Read the docs:
[vim-plug](https://github.com/junegunn/vim-plug)
