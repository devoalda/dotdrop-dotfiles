------------------------------------------------------------------------
---IMPORTS
------------------------------------------------------------------------
    -- Base
import XMonad
import XMonad.Config.Desktop
import System.IO (hPutStrLn)
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W

    -- Prompt
import XMonad.Prompt
import XMonad.Prompt.Man
import XMonad.Prompt.Pass
import XMonad.Prompt.Shell (shellPrompt)
import XMonad.Prompt.Ssh
import XMonad.Prompt.XMonad
import Control.Arrow ((&&&),first)

    -- Data
import Data.List
import Data.Monoid
import Data.Maybe (isJust)
import qualified Data.Map as M

    -- Utilities
import XMonad.Util.Loggers
import XMonad.Util.EZConfig (additionalKeysP, additionalMouseBindings)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (safeSpawn, unsafeSpawn, runInTerm, spawnPipe)
import XMonad.Util.SpawnOnce
import XMonad.Util.Cursor

    -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, defaultPP, wrap, pad, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.ManageDocks (avoidStruts, docksStartupHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, isDialog,  doFullFloat, doCenterFloat)
import XMonad.Hooks.Place (placeHook, withGaps, smart)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.EwmhDesktops

    -- Actions
import XMonad.Actions.Minimize (minimizeWindow)
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.CopyWindow (kill1, copyToAll, killAllOtherCopies, runOrCopy)
import XMonad.Actions.WindowGo (runOrRaise, raiseMaybe)
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Actions.CycleWS (moveTo, shiftTo, WSType(..), nextScreen, prevScreen, shiftNextScreen, shiftPrevScreen)
import XMonad.Actions.GridSelect
import XMonad.Actions.DynamicWorkspaces (addWorkspacePrompt, removeEmptyWorkspace)
import XMonad.Actions.MouseResize
import qualified XMonad.Actions.ConstrainedResize as Sqr

    -- Layouts modifiers
import XMonad.Layout.Gaps
--import XMonad.Layout.PerWorkspace (onWorkspace)
import XMonad.Layout.Renamed (renamed, Rename(CutWordsLeft, Replace))
import XMonad.Layout.WorkspaceDir
import XMonad.Layout.Spacing (spacing)
import XMonad.Layout.NoBorders
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.Reflect (reflectVert, reflectHoriz, REFLECTX(..), REFLECTY(..))
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), Toggle(..), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import XMonad.Layout.IndependentScreens

    -- Layouts
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.OneBig
import XMonad.Layout.ThreeColumns
import XMonad.Layout.ResizableTile
import XMonad.Layout.ZoomRow (zoomRow, zoomIn, zoomOut, zoomReset, ZoomMessage(ZoomFullToggle))
import XMonad.Layout.IM (withIM, Property(Role))
import XMonad.Layout.Spacing

   -- Colors
import Control.Monad(when)
import Data.List
import System.Directory
import System.Environment
import System.Exit
import System.IO

getConfigFilePath f =
  getHomeDirectory >>= \hd -> return $ hd ++ "/" ++ f

getWalColors = do
  file <- getConfigFilePath ".cache/wal/colors"
  contents <- readFile file
  let colors = lines contents
  return (colors ++ (replicate (16 - length colors) "#000000"))

------------------------------------------------------------------------
---VARIABLES
------------------------------------------------------------------------
myFont          = "xft:JetBrains Mono NL:regular:pixelsize=12"
myModMask       = mod4Mask     -- Sets modkey to super/windows key
altMask         = mod4Mask     --this is the super key, but I have it remapped
myTerminal      = "st"         -- Sets default terminal
myTextEditor    = "nvim"       -- Sets default text editor
myBorderWidth   = 0            -- Sets border width for windows
windowCount     = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myGaps :: Int
myGaps = 5                 -- Sets layout gaps and window spacing


------------------------------------------------------------------------
---AUTOSTART
------------------------------------------------------------------------
myStartupHook = do
	  -- spawnOnce "wallpaper &"
	  -- spawnOnce "flameshot &"
	  -- spawnOnce "picom -b"
	  spawnOnce "trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --transparent true --tint 0x000000 --height 18 &"
	  spawnOnce "fcitx &"
          setDefaultCursor xC_left_ptr
          --setWMName "LG3D"

--------------------------------------------------------------------------
---GRID SELECT
------------------------------------------------------------------------
myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                  (0x31,0x2e,0x39) -- lowest inactive bg
                  (0x31,0x2e,0x39) -- highest inactive bg
                  (0x61,0x57,0x72) -- active bg
                  (0xc0,0xa7,0x9a) -- inactive fg
                  (0xff,0xff,0xff) -- active fg

-- gridSelect menu layout
mygridConfig colorizer = (buildDefaultGSConfig myColorizer)
    { gs_cellheight   = 30
    , gs_cellwidth    = 200
    , gs_cellpadding  = 8
    , gs_originFractX = 0.5
    , gs_originFractY = 0.5
    , gs_font         = myFont
    }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = defaultGSConfig

------------------------------------------------------------------------
-- XPROMPT KEYMAP (emacs-like key bindings)
------------------------------------------------------------------------
--myXPKeymap :: M.Map (KeyMask,KeySym) (XP ())
--myXPKeymap = M.fromList $
  --map (first $ (,) controlMask)   -- control + <key>
  --[ (xK_z, killBefore)            -- kill line backwards
  --, (xK_k, killAfter)             -- kill line fowards
  --, (xK_a, startOfLine)           -- move to the beginning of the line
  --, (xK_e, endOfLine)             -- move to the end of the line
  --, (xK_m, deleteString Next)     -- delete a character foward
  --, (xK_b, moveCursor Prev)       -- move cursor forward
  --, (xK_f, moveCursor Next)       -- move cursor backward
  --, (xK_BackSpace, killWord Prev) -- kill the previous word
  --, (xK_y, pasteString)           -- paste a string
  --, (xK_g, quit)                  -- quit out of prompt
  --, (xK_bracketleft, quit)
  --] ++
  --map (first $ (,) altMask)       -- meta key + <key>
  --[ (xK_BackSpace, killWord Prev) -- kill the prev word
  --, (xK_f, moveWord Next)         -- move a word forward
  --, (xK_b, moveWord Prev)         -- move a word backward
  --, (xK_d, killWord Next)         -- kill the next word
  --, (xK_n, moveHistory W.focusUp')
  --, (xK_p, moveHistory W.focusDown')
  --] ++
  --map (first $ (,) 0) -- <key>
  --[ (xK_Return, setSuccess True >> setDone True)
  --, (xK_KP_Enter, setSuccess True >> setDone True)
  --, (xK_BackSpace, deleteString Prev)
  --, (xK_Delete, deleteString Next)
  --, (xK_Left, moveCursor Prev)
  --, (xK_Right, moveCursor Next)
  --, (xK_Home, startOfLine)
  --, (xK_End, endOfLine)
  --, (xK_Down, moveHistory W.focusUp')
  --, (xK_Up, moveHistory W.focusDown')
  --, (xK_Escape, quit)
  --]

------------------------------------------------------------------------
-- XPROMPT SETTINGS
------------------------------------------------------------------------
myXPConfig = def
      { font                  = "xft:Hack:size=10"
        , bgColor             = "#292d3e"
        , fgColor             = "#d0d0d0"
        , bgHLight            = "#c792ea"
        , fgHLight            = "#000000"
        , borderColor         = "#535974"
        , promptBorderWidth   = 0
        --, promptKeymap        = myXPKeymap
        , position            = Top
--        , position            = CenteredAt { xpCenterY = 0.3, xpWidth = 0.3 }
        , height              = 20
        , historySize         = 256
        , historyFilter       = id
        , defaultText         = []
        , autoComplete        = Just 100000    -- set Just 100000 for .1 sec
        , showCompletionOnTab = False
        , searchPredicate     = isPrefixOf
        , alwaysHighlight     = True
        , maxComplRows        = Nothing        -- set to Just 5 for 5 rows
        }
------------------------------------------------------------------------
---KEYBINDINGS
------------------------------------------------------------------------
myKeys =
    -- Xmonad
        [ ("M-C-r", spawn "xmonad --recompile")      -- Recompiles xmonad
        , ("M-S-r", spawn "xmonad --restart")        -- Restarts xmonad
        , ("M-C-e", io exitSuccess)                  -- Quits xmonad

    -- Prompts
        , ("M-S-<Return>", shellPrompt myXPConfig)   -- Shell Prompt
        , ("M-S-o", xmonadPrompt myXPConfig)         -- Xmonad Prompt
        , ("M-S-s", sshPrompt myXPConfig)            -- Ssh Prompt
        , ("M-S-m", manPrompt myXPConfig)            -- Manpage Prompt

    -- Windows
        , ("M-S-q", kill1)                           -- Kill the currently focused client
        , ("M-S-a", killAll)                         -- Kill all the windows on current workspace

    -- Dmenu
        , ("M-d", spawn "dmenu_run")

    -- Floating windows
        , ("M-<Delete>", withFocused $ windows . W.sink) -- Push floating window back to tile.
        , ("M-S-<Delete>", sinkAll)                      -- Push ALL floating windows back to tile.

    -- Grid Select
        , (("M-S-t"), spawnSelected'
          [ ("Audacity", "audacity")
          , ("Deadbeef", "deadbeef")
          , ("Emacs", "emacs")
          , ("Firefox", "firefox")
          , ("Geany", "geany")
          , ("Geary", "geary")
          , ("Gimp", "gimp")
          , ("Kdenlive", "kdenlive")
          , ("LibreOffice Impress", "loimpress")
          , ("LibreOffice Writer", "lowriter")
          , ("OBS", "obs")
          , ("PCManFM", "pcmanfm")
          --spawnOnce "volumeicon &"
          , ("Simple Terminal", "st")
          , ("Steam", "steam")
          , ("Surf Browser",    "surf suckless.org")
          , ("Xonotic", "xonotic-glx")
          ])

        , ("M-S-g", goToSelected $ mygridConfig myColorizer)
        , ("M-S-b", bringSelected $ mygridConfig myColorizer)

    -- Windows navigation
        , ("M-m", windows W.focusMaster)             -- Move focus to the master window
        , ("M-j", windows W.focusDown)               -- Move focus to the next window
        , ("M-k", windows W.focusUp)                 -- Move focus to the prev window
        --, ("M-S-m", windows W.swapMaster)            -- Swap the focused window and the master window
        , ("M-S-j", windows W.swapDown)              -- Swap the focused window with the next window
        , ("M-S-k", windows W.swapUp)                -- Swap the focused window with the prev window
        , ("M-<Backspace>", promote)                 -- Moves focused window to master, all others maintain order
        , ("M1-S-<Tab>", rotSlavesDown)              -- Rotate all windows except master and keep focus in place
        , ("M1-C-<Tab>", rotAllDown)                 -- Rotate all the windows in the current stack
        --, ("M-S-s", windows copyToAll)
        , ("M-C-s", killAllOtherCopies)

        , ("M-C-M1-<Up>", sendMessage Arrange)
        , ("M-C-M1-<Down>", sendMessage DeArrange)
        , ("M-<Up>", sendMessage (MoveUp 10))             --  Move focused window to up
        , ("M-<Down>", sendMessage (MoveDown 10))         --  Move focused window to down
        , ("M-<Right>", sendMessage (MoveRight 10))       --  Move focused window to right
        , ("M-<Left>", sendMessage (MoveLeft 10))         --  Move focused window to left
        , ("M-S-<Up>", sendMessage (IncreaseUp 10))       --  Increase size of focused window up
        , ("M-S-<Down>", sendMessage (IncreaseDown 10))   --  Increase size of focused window down
        , ("M-S-<Right>", sendMessage (IncreaseRight 10)) --  Increase size of focused window right
        , ("M-S-<Left>", sendMessage (IncreaseLeft 10))   --  Increase size of focused window left
        , ("M-C-<Up>", sendMessage (DecreaseUp 10))       --  Decrease size of focused window up
        , ("M-C-<Down>", sendMessage (DecreaseDown 10))   --  Decrease size of focused window down
        , ("M-C-<Right>", sendMessage (DecreaseRight 10)) --  Decrease size of focused window right
        , ("M-C-<Left>", sendMessage (DecreaseLeft 10))   --  Decrease size of focused window left

    -- Layouts
        , ("M-<Tab>", sendMessage NextLayout)                                -- Switch to next layout
        , ("M-S-<Space>", sendMessage ToggleStruts)                          -- Toggles struts
        , ("M-S-n", sendMessage $ Toggle NOBORDERS)                          -- Toggles noborder
        , ("M-S-=", sendMessage (Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
        , ("M-S-f", sendMessage (T.Toggle "float"))
        , ("M-S-x", sendMessage $ Toggle REFLECTX)
        , ("M-S-y", sendMessage $ Toggle REFLECTY)
        --, ("M-S-m", sendMessage $ Toggle MIRROR)
        , ("M-<KP_Multiply>", sendMessage (IncMasterN 1))   -- Increase number of clients in the master pane
        , ("M-<KP_Divide>", sendMessage (IncMasterN (-1)))  -- Decrease number of clients in the master pane
        , ("M-S-<KP_Multiply>", increaseLimit)              -- Increase number of windows that can be shown
        , ("M-S-<KP_Divide>", decreaseLimit)                -- Decrease number of windows that can be shown

        , ("M-h", sendMessage Shrink)
        , ("M-l", sendMessage Expand)
        , ("M-C-j", sendMessage MirrorShrink)
        , ("M-C-k", sendMessage MirrorExpand)
        , ("M-S-;", sendMessage zoomReset)
        , ("M-;", sendMessage ZoomFullToggle)

    -- Workspaces
        , ("M-.", nextScreen)                           -- Switch focus to next monitor
        , ("M-,", prevScreen)                           -- Switch focus to prev monitor
        , ("M-S-<KP_Add>", shiftTo Next nonNSP >> moveTo Next nonNSP)       -- Shifts focused window to next workspace
        , ("M-S-<KP_Subtract>", shiftTo Prev nonNSP >> moveTo Prev nonNSP)  -- Shifts focused window to previous workspace

    -- Scratchpads
        , ("M-C-<Return>", namedScratchpadAction myScratchPads "terminal")
        , ("M-C-c", namedScratchpadAction myScratchPads "cmus")

    -- Open My Preferred Terminal.
        , ("M-<Return>", spawn (myTerminal ))

    --- My Applications
        , ("M-M1-r", spawn (myTerminal ++ " -e ranger"))
        , ("M-M1-n", spawn (myTerminal ++ " -e ncmpcpp"))
        , ("M-M1-l", spawn "mpd_lyrics")
        , ("M-M1-g", spawn (myTerminal ++ " -e geniuslyrics | less"))
        , ("M-M1-a", spawn (myTerminal ++ " -e pulsemixer"))
        , ("M-M1-j", spawn (myTerminal ++ " -e joplin"))
        , ("M-M1-h", spawn (myTerminal ++ " -e htop"))
        , ("M-M1-f", spawn (myTerminal ++ " -e newsboat"))
        , ("M-M1-m", spawn "mpv_weblink audio")
        , ("M-M1-v", spawn "mpv_weblink video")
        , ("M-M1-s", spawn "pulsecontrol next-sink")
        , ("M-M1-b", spawn "btmenu")
        , ("M-M1-e", spawn "dmenuunicode")
        , ("M-M1-w", spawn "networkmanager_dmenu")
        , ("M-M1-p", spawn "multiscreen")

        , ("M-<F1>", spawn "wallpaper")
        , ("M-<F2>", spawn "fav-wallpaper")
        , ("M-<F3>", spawn (myTerminal ++ " -e wallpaper_pick"))
        , ("M-<F6>", spawn "dmenu_edit_configs")
        , ("M-<F7>", spawn "dmenu_applications")
	, ("M-<F11>", spawn "betterlockscreen -u $HOME/Documents/pictures/general -l dim")
	, ("M-<F12>", spawn "dmenu_power")

    -- Multimedia Keys
        , ("<XF86Calculator>", spawn (myTerminal ++ " -e bc -l"))
        , ("<XF86AudioPlay>", spawn "mpc toggle")
        , ("<XF86AudioPrev>", spawn "mpc prev")
        , ("<XF86AudioNext>", spawn "mpc next")
        , ("<XF86AudioStop>", spawn "mpc stop")
        , ("<XF86AudioMute>", spawn "pamixer -t")
        , ("<XF86AudioLowerVolume>", spawn "pamixer --allow-boost -d 10")
        , ("<XF86AudioRaiseVolume>", spawn "pamixer --allow-boost -i 10")
        , ("<Print>", spawn "flameshot gui")
        ] where nonNSP          = WSIs (return (\ws -> W.tag ws /= "nsp"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "nsp"))

------------------------------------------------------------------------
---WORKSPACES
------------------------------------------------------------------------
-- My workspaces are clickable meaning that the mouse can be used to switch
-- workspaces. This requires xdotool.

xmobarEscape = concatMap doubleLts
  where
        doubleLts '<' = "<<"
        doubleLts x   = [x]

myWorkspaces :: [String]
myWorkspaces = clickable . (map xmobarEscape)
               $ ["ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE"]
  where
        clickable l = [ "<action=xdotool key super+" ++ show (n) ++ ">" ++ ws ++ "</action>" |
                      (i,ws) <- zip [1..9] l,
                      let n = i ]
------------------------------------------------------------------------
-- MANAGEHOOK
------------------------------------------------------------------------
-- Sets some rules for certain programs. Examples include forcing certain
-- programs to always float, or to always appear on a certain workspace.
-- Forcing programs to a certain workspace with a doShift requires xdotool
-- if you are using clickable workspaces. You need the className or title
-- of the program. Use xprop to get this info.

myManageHook :: Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     [className =? "Nightly"     --> doShift "<action=xdotool key super+1>1</action>"
      , className=? "plexmediaplayer"     --> doShift ( myWorkspaces !! 3)
      , className=? "Spotify"     --> doShift ( myWorkspaces !! 3)
      , className=? "firefox"     --> doShift ( myWorkspaces !! 0)
      , className=? "VSCodium"     --> doShift ( myWorkspaces !! 2)
      , className=? "Spotify"     --> doShift ( myWorkspaces !! 3)
      , className=? "Steam"     --> doShift ( myWorkspaces !! 4)
      , className=? "Vmware"     --> doShift ( myWorkspaces !! 8)
      , className=? "Hamsket"     --> doShift ( myWorkspaces !! 7)
      , className=? "realvnc-vncserverui-service"     --> doShift ( myWorkspaces !! 9)
      , className =? "mpv"        --> doFloat
      , className =? "imv"        --> doFloat
      , (className =? "firefox" <&&> resource =? "Toolkit") --> doFloat
      , (className =? "firefox" <&&> resource =? "Places") --> doFloat
      , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
     ] <+> namedScratchpadManageHook myScratchPads

------------------------------------------------------------------------
-- LAYOUTS
------------------------------------------------------------------------
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats $
               mkToggle (NBFULL ?? NOBORDERS ?? EOT) $ myDefaultLayout
             where
                 myDefaultLayout = tall ||| grid ||| threeCol ||| threeRow ||| oneBig ||| noBorders monocle ||| space ||| floats

tall     = renamed [Replace "tall"]     $ limitWindows 12 $ gaps [(U,myGaps), (D,myGaps), (L,myGaps), (R,myGaps)] $ spacing myGaps $ ResizableTall 1 (3/100) (1/2) []
grid     = renamed [Replace "grid"]     $ limitWindows 12 $ spacing myGaps $ mkToggle (single MIRROR) $ Grid (16/10)
threeCol = renamed [Replace "threeCol"] $ limitWindows 3  $ ThreeCol 1 (3/100) (1/2)
threeRow = renamed [Replace "threeRow"] $ limitWindows 3  $ Mirror $ mkToggle (single MIRROR) zoomRow
oneBig   = renamed [Replace "oneBig"]   $ limitWindows 6  $ Mirror $ mkToggle (single MIRROR) $ mkToggle (single REFLECTX) $ mkToggle (single REFLECTY) $ OneBig (5/9) (8/12)
monocle  = renamed [Replace "monocle"]  $ limitWindows 20 $ Full
space    = renamed [Replace "space"]    $ limitWindows 4  $ spacing 12 $ Mirror $ mkToggle (single MIRROR) $ mkToggle (single REFLECTX) $ mkToggle (single REFLECTY) $ OneBig (2/3) (2/3)
floats   = renamed [Replace "floats"]   $ limitWindows 20 $ simplestFloat



------------------------------------------------------------------------
-- SCRATCHPADS
------------------------------------------------------------------------
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "cmus" spawnCmus findCmus manageCmus
                ]
    where
    spawnTerm  = myTerminal ++  " -n scratchpad"
    findTerm   = resource =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
                 where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnCmus  = myTerminal ++  " -n cmus 'cmus'"
    findCmus   = resource =? "cmus"
    manageCmus = customFloating $ W.RationalRect l t w h
                 where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
------------------------------------------------------------------------
---MAIN
------------------------------------------------------------------------
main = do
    nScreens <- countScreens
    colors <- getWalColors
    xmproc <- spawnPipe "xmobar ~/.xmonad/xmobarrc"
    xmonad $ ewmh $ desktopConfig
        { manageHook = ( isFullscreen --> doFullFloat ) <+> manageDocks <+> myManageHook <+> manageHook desktopConfig
        , modMask            = myModMask
        , terminal           = myTerminal
	, startupHook        = myStartupHook
        , layoutHook         = myLayoutHook
	, workspaces         = myWorkspaces
        , borderWidth        = myBorderWidth
	, normalBorderColor  = colors!!10
	, focusedBorderColor = colors!!12
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppCurrent = xmobarColor (colors!!14) "" . wrap "[" "]" -- Current workspace in xmobar
                        , ppVisible = xmobarColor  (colors!!13) ""                -- Visible but not current workspace
                        , ppHidden = xmobarColor (colors!!15) "" . wrap "*" ""   -- Hidden workspaces in xmobar
                        , ppHiddenNoWindows = xmobarColor (colors!!11) ""        -- Hidden workspaces (no windows)
                        , ppTitle = xmobarColor (colors!!14) "" . shorten 60     -- Title of active window in xmobar
			, ppSep =  "<fc=" ++ (colors!!2) ++ "> :: </fc>"                     -- Separators in xmobar
                        , ppUrgent = xmobarColor  (colors!!15) "" . wrap "!" "!"  -- Urgent workspace
                        , ppExtras  = [windowCount]                           -- # of windows current workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                        }
        } `additionalKeysP`         myKeys


