<!-- Badges -->

[![Bash Shell](https://badges.frapsoft.com/bash/v1/bash.png?v=103)](https://github.com/ellerbrock/open-source-badges/)

<!-- PROJECT LOGO -->

![Wolf Logo](https://i.pinimg.com/originals/5e/0a/ac/5e0aacf4c75613ec814457d520050c88.jpg)

# Dotfiles By Devoalda

Devoalda's .dotfiles
\
[Explore the docs](./readme.md)

## Table of Contents

- [Introduction](#introduction)
- [Setup Repository](#setup-repository)
- [Track Files](#track-files)
- [Restore Configurations](#restore-configurations)
- [Applications](#applications-and-key-bindings)

## Introduction

This repository contains my personal configuration files managed with [dotdrop](https://github.com/deadc0de6/dotdrop)

## Setup Repository

Clone the repository to your machine and install

```bash
git clone --recurse-submodules https://gitlab.com/devoalda/dotdrop-dotfiles.git
pip3 install --user -r ./dotdrop-dotfiles/dotdrop/requirements.txt
./dotdrop-dotfiles/dotdrop.sh install -p carnage
```

## Track Files

```bash
dotgit fetch
dotgit pull
```

## Restore Configurations

### Run scripts in ~/.local/bin/

Assuming ~/.local/bin/ is in path

```bash
install-base
```

or

```bash
install-packages
```

## Applications and Key Bindings

| Item                     | Application/Dependencies                                                                   | Keybindings          | Action/Remarks                                                                          |
| ------------------------ | ------------------------------------------------------------------------------------------ | -------------------- | --------------------------------------------------------------------------------------- |
| Distribution             | Arch Linux                                                                                 | `NIL`                | I use Arch BTW                                                                          |
| Window Manager           | Xmonad                                                                                     | `MODKEY + Shift + r` | Restart Window Manager                                                                  |
| Window Manager           | Xmonad                                                                                     | `MODKEY + CTRL + e`  | Exit Window Manager                                                                     |
| Status Bar               | Xmobar                                                                                     | `NIL`                | Here are the [Scripts](./dotfiles/local/bin/) I use                                     |
| Application Launcher     | Fork of dmenu - [dmenu](https://github.com/LukeSmithxyz/dmenu)                             | `MODKEY + d`         | Opens dmenu                                                                             |
| Shell                    | ZSH                                                                                        | `NIL`                | Shell opens in terminal emulator                                                        |
| Terminal Emulator        | [st-luke-git](https://aur.archlinux.org/packages/st-luke-git/)                             | `MODKEY + RETURN`    | Opens st                                                                                |
| System Monitor           | htop fork with vim keybinds - [htop-vim](https://github.com/KoffeinFlummi/htop-vim)        | `MODKEY + ALT + h`   | Launches htop with vim keybindings in terminal                                          |
| Browser                  | Firefox                                                                                    | `NIL`                | Use Dmenu/application launcher to launch                                                |
| File Browser             | Ranger                                                                                     | `MODKEY + ALT + r`   | Launches Ranger in default terminal emulator                                            |
| Text Editor (terminal)   | Neovim                                                                                     | `NIL`                | Launch Neovim in terminal emulator                                                      |
| Text Editor (GUI)        | [VSCodium](https://github.com/VSCodium/vscodium)                                           | `NIL`                | Launch VSCodium in Dmenu/application launcher                                           |
| Sound Mixer/Control      | Pulseaudio, Pavucontrol, Pulsemixer                                                        | `MODKEY + ALT + a`   | Launches Pulsemixer in terminal                                                         |
| Network Manager          | NetworkManager & [networkmanager_dmenu](https://github.com/firecat53/networkmanager-dmenu) | `MODKEY + ALT + w`   | Launches networkmanager_dmenu                                                           |
| Bluetooth Manager        | bluetoothctl & [btmenu](https://aur.archlinux.org/packages/btmenu/)                        | `MODKEY + ALT + b`   | Launches btmenu                                                                         |
| Clipboard Manager        | xclip                                                                                      | `NIL`                | Clipboard Manager                                                                       |
| Notification Manager     | Dunst                                                                                      | `NIL`                | Notification Manager                                                                    |
| Keyboard Manager         | Fcitx                                                                                      | `CTRL + Space`       | Trigger Input Method                                                                    |
| Keyboard Manager         | Fcitx                                                                                      | `CTRL + SHIFT`       | Switch Input Methods                                                                    |
| Screenshot               | flameshot                                                                                  | `PrintSc`            | Launches flameshot gui                                                                  |
| Tray Application Manager | trayer                                                                                     | `NIL`                | Tray icons displayed in trayer                                                          |
| Music Player             | MPD with Ncmpcpp                                                                           | `MODKEY + ALT + n`   | Launches ncmpcpp in terminal                                                            |
| Media Player             | MPV                                                                                        | `NIL`                | Launch MPV in terminal/ranger                                                           |
| Music Player             | Spotify with [Spicetify](https://github.com/khanhas/spicetify-cli)                         | `NIL`                | Launch Spotify from dmenu                                                               |
| RSS Reader               | Newsboat                                                                                   | `NIL`                | Launch Newsboat from terminal                                                           |
| Note Taking Application  | Joplin                                                                                     | `NIL`                | Launch Joplin from terminal                                                             |
| Lockscreen               | [Betterlockscreen](https://github.com/pavanjadhaw/betterlockscreen)                        | `MODKEY + F11`       | Locks screen with Betterlockscreen                                                      |
| ColorScheme              | [Pywal](https://github.com/dylanaraps/pywal)                                               | `MODKEY + F1`        | Runs [wallpaper](./dotfiles/local/bin/wallpaper) command to switch colorschemes         |
| Compositor               | Fork of picom - [picom-ibhagwan-git](https://github.com/ibhagwan/picom)                    | `NIL`                | Compositor                                                                              |
| Cursor                   | [Babita ice](https://github.com/KaizIqbal/Bibata_Cursor)                                   | `NIL`                | Cursor                                                                                  |
| Icon Theme               | Arc                                                                                        | `NIL`                | Icon                                                                                    |
| Font                     | Hack                                                                                       | `NIL`                | Base Font                                                                               |
| Font                     | JoyPixels                                                                                  | `MODKEY + ALT + e`   | Launch [dmenuunicode](./dotfiles/local/bin/dmenu_scripts/dmenuunicode) to select Emojis |
| Font                     | KoPubWorldBatang                                                                           | `NIL`                | Chinese, Korean, Japanese Characters Support                                            |

## Note

I use pywal for dynamic theming of my system, Here are the applications themed by pywal

- Xmonad
- Xmobar
- Firefox
- Spotify (Spicetify)
- Discord-Canary
- Zathura
- Dunst
- Terminal Based applications
  - Ranger
  - Ncmpcpp
  - Cava

### Pywal Templates

I have pywal templates for the following applications:

- Cava
- Dunst
- Xmobar
- Zathura

All templates in [templates folder](./dotfiles/config/wal/templates)

### Pywal Scripts

Theme/Wallpaper is updated with the [wallpaper](./dotfiles/local/bin/wallpaper) script. The script will reconfigure:

- Xmonad
- Xmobar (Separate script used, but used in wallpaper script)
- firefox (pywalfox)
- discord (pywal-discord)
- spotify (spicetify)

Terminal based applications will automatically reload when wallpaper script is executed.
